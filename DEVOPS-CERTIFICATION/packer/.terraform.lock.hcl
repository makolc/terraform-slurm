# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:VSnd9ZIPyfKHOObuQCaKfnjIHRtR7qTw19Rz8tJxm+k=",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.35.0"
  constraints = "~> 1.35.0"
  hashes = [
    "h1:k1SCosvSICWAgRkswl83KtCycN7iP9asejWDDEQEtuk=",
  ]
}
