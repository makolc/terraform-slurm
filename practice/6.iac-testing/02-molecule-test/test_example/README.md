#### Ответы на домашнее задание
* Ошибка на стадии prepare

Замените файл `molecule/default/prepare.yml` на [prepare.yml](prepare.yml)

* Ошибка тестов, проверка selinux

В конец файла: необходимо добавить

```yaml
- name: Reboot to apply selinus
  reboot:
    reboot_timeout: 3600
  when: selinux_change is changed

- name: Wait for system to become reachable
  wait_for_connection:
    timeout: 900
  when: selinux_change is changed
```

После `14` строки добавить:

```yaml
  register: selinux_change
```
