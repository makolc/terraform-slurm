`create_server/vars.tf`
```terraform
variable "server_count" {
  default = 1
}
```

`create_server/main.tf`
```terraform
resource "openstack_networking_port_v2" "port_1" {
  name       = "node-${count.index}-eth0"
  count      = var.server_count
...

resource "openstack_blockstorage_volume_v3" "volume_1" {
  name                 = "volume-for-node-${count.index}"
  count                = var.server_count
...

resource "openstack_compute_instance_v2" "instance_1" {
  name              = "node-${count.index}"
  count             = var.server_count
  network {
    port = openstack_networking_port_v2.port_1[count.index].id
  }

  block_device {
    uuid             = openstack_blockstorage_volume_v3.volume_1[count.index].id
...
```
