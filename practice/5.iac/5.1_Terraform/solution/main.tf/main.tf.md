`main.tf`

```tf
resource "openstack_compute_flavor_v2" "flavor-node" {
  name      = "node.${var.project_id}-${random_string.random_name_1.result}"
  ram       = var.ram
  vcpus     = "1"
  disk      = "0"
  is_public = "false"
}
...
resource "openstack_compute_keypair_v2" "terraform_key" {
  name       = "terraform_key-${random_string.random_name_1.result}"
  region     = var.region
  public_key = file("~/.ssh/id_rsa.pub")
}
```

`outputs.tf`
```tf
output "server_internal_ip" {
  value = openstack_compute_instance_v2.instance_1.access_ip_v4
}
```
`vars.tf`
```tf
variable "ram" {
  default = 1024
}
```
